namespace EnvironmentVariablesExample.Models
{
    public class EnvironmentVariableSetting
    {
        public string AccountManagementConnectionString {get;set;}
        public string DeviceManagementConnectionString {get;set;}
        public string EventManagementConnectionString {get;set;}
    }
}