using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using EnvironmentVariablesExample.Models;
namespace EnvironmentVariablesExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserEnvironmentVariablesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            var environmentVariables = Environment.GetEnvironmentVariables(EnvironmentVariableTarget.User);
            return Ok(environmentVariables);
        }

        // GET api/values/5
        [HttpGet("{name}")]
        public IActionResult Get(string name)
        {
            DictionaryEntry result ;
            foreach (DictionaryEntry de in Environment.GetEnvironmentVariables(EnvironmentVariableTarget.User))
            {
                if(de.Key.ToString() == name)
                    result = de;
            }
            if(result.Key == null)
                return NotFound($"Not found {name}");
            return Ok(result);
        }
    }
}
